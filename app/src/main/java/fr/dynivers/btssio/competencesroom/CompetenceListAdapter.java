package fr.dynivers.btssio.competencesroom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CompetenceListAdapter extends RecyclerView.Adapter<CompetenceListAdapter.CompetenceViewHolder> {
    class CompetenceViewHolder extends RecyclerView.ViewHolder {
        private final TextView competenceItemView;
        private CompetenceViewHolder(View view) {
            super(view);
            competenceItemView = view.findViewById(R.id.textView_compentence);
        }
    }

    private final LayoutInflater inflater;
    private List<Competence> competences;
    CompetenceListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public CompetenceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.recyclerview_item, parent, false);
        return new CompetenceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CompetenceViewHolder holder, int position) {
        if(competences != null) {
            Competence competence = competences.get(position);
            holder.competenceItemView.setText(competence.getNomCompetence());
        }
        else {
            holder.competenceItemView.setText("Aucune compétence à afficher");
        }
    }

    @Override
    public int getItemCount() {
        if(competences != null) return competences.size();
        else return 0;
    }

    void setCompetences(List<Competence> competences) {
        this.competences = competences;
        notifyDataSetChanged();
    }
}
