package fr.dynivers.btssio.competencesroom;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class CompetenceViewModel extends AndroidViewModel {
    private CompetenceRepository competenceRepository;

    private LiveData<List<Competence>> competences;

    public CompetenceViewModel (Application application) {
        super(application);
        competenceRepository = new CompetenceRepository(application);
        competences = competenceRepository.getCompetences();
    }

    LiveData<List<Competence>> getCompetences() {
        return competences;
    }

    public void insert(Competence competence) {
        competenceRepository.insert(competence);
    }
}
