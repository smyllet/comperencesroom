package fr.dynivers.btssio.competencesroom;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class CompetenceRepository {
    private CompetenceDao competenceDao;
    private LiveData<List<Competence>> competences;

    CompetenceRepository(Application application) {
        CompetenceRoomDatabase database = CompetenceRoomDatabase.getDatabase(application);
        competenceDao = database.competenceDao();
        competences = competenceDao.getToutesCompetences();
    }

    LiveData<List<Competence>> getCompetences() {
        return competences;
    }

    public void insert (Competence competence) {
        new insertAsyncTask(competenceDao).execute(competence);
    }

    private static class insertAsyncTask extends AsyncTask<Competence, Void, Void> {
        private CompetenceDao tacheDao;

        insertAsyncTask(CompetenceDao dao) {
            tacheDao = dao;
        }

        @Override
        protected Void doInBackground(final Competence... params) {
            tacheDao.insert(params[0]);
            return null;
        }
    }
}
